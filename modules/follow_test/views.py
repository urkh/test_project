from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from modules.follow_test.models import UserProfile
from follow.models import Follow
from follow import utils

@login_required
def my_followers(request):
    user = User.objects.get(id=request.user.id)
    followers = Follow.objects.get_follows(user)
    return render_to_response('my_followers.html', locals(), context_instance=RequestContext(request))

@login_required
def my_home(request):
    if request.method == 'POST':
        uprofile = UserProfile.objects.create(
            user = request.user,
            text = request.POST['text']
        )
        uprofile.save()
    uprofile = UserProfile.objects.filter(user=request.user).order_by('-pk')
    return render_to_response('my_home.html', locals(), context_instance=RequestContext(request))

@login_required
def user_profile(request):

    user = User.objects.get(id=request.GET['user_id'])
    if Follow.objects.is_following(request.user, user):
        uprofile = UserProfile.objects.filter(user_id=request.GET['user_id']).order_by('-pk')
    return render_to_response('user_profile.html', locals(), context_instance=RequestContext(request))
