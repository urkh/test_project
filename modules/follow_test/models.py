from django.db import models
from follow import utils
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.ForeignKey(User)
    text = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s - %s %s'%(self.user, self.text, self.date)

utils.register(User)
