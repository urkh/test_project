from django import template
from django.contrib.auth.models import User
from follow.models import Follow
register = template.Library()

@register.simple_tag(takes_context=True)
def users_list(context):
    request = context['request']
    users = User.objects.all()
    data = []

    for user in users:
        if request.user.id != user.id:
            if Follow.objects.is_following(request.user, user):
                data.append("<button id='%s' onclick='unfollow(%s)' class='btn btn-default active'>Following %s</button>  <a href='/follow_test/user_profile/?user_id=%s'>view profile</a><br><br>"%(user.id, user.id, user.username, user.id))
            else:
                data.append("<button id='%s' onclick='follow(%s)' class='follow_button btn btn-default'>Follow %s</button>  <a href='/follow_test/user_profile/?user_id=%s'>view profile</a><br><br>"%(user.id, user.id, user.username, user.id))

    return "".join(data)
