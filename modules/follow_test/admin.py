from django.contrib import admin
from modules.follow_test.models import UserProfile



class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user','text','date']
admin.site.register(UserProfile, UserProfileAdmin)
