from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    "",
    url(r"^my_followers/", "modules.follow_test.views.my_followers", name="my_followers"),
    url(r"^my_home/", "modules.follow_test.views.my_home", name="my_home"),
    url(r"^user_profile/", "modules.follow_test.views.user_profile", name="user_profile"),
)
