from importlib import import_module

from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):

    name = "test_project"

    def ready(self):
        import_module("test_project.receivers")
