function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var token = getCookie('csrftoken');

function follow(user_id){
    $.ajax({
        type: 'POST',
        url: '/follow/toggle/auth/user/'+user_id+'/',
        data: {
            csrfmiddlewaretoken: token                            
        },
        dateType: 'json',
        
        success: function (resp) {
            var arr = $('#'+user_id)[0].innerText.split(" ");
            $('#'+user_id)[0].onclick = function(){unfollow(user_id)};
            $('#'+user_id)[0].innerText = "Following "+arr[1];
            $('#'+user_id).addClass('active');
        }
    });
}


function unfollow(user_id){
    $.ajax({
        type: 'POST',
        url: '/follow/toggle/auth/user/'+user_id+'/',
        data: {
            csrfmiddlewaretoken: token                            
        },
        dateType: 'json',
        success: function (resp) {
            var arr = $('#'+user_id)[0].innerText.split(" ");
            $('#'+user_id)[0].onclick = function(){follow(user_id)};
            $('#'+user_id)[0].innerText = "Follow "+arr[1];
            $('#'+user_id).removeClass('active');
        }
    });
}
